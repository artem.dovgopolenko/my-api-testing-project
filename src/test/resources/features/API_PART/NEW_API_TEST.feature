Feature: New api feature

  Scenario: 01. Check that responce body /quotes is sent and responce code is 200
    When Send GET request with "/quotes" url and check "200" code

  Scenario: 02. Check that responce body /symbols is sent and responce code is 200
   When Send GET request with "/symbols" url and check "200" code

  Scenario: 03. Check that responce body of unexistant page /test12345 is not sent and responce code is 404
    When Send GET request with "/test12345" url and check "404" code

  Scenario: 04. Check that POST request is unable on page /quotes
   Then Send POST request using "test.json" json file and "/quotes" url and check "404" code

  Scenario: 05. Check that POST request is unable on page /symbols
    Then Send POST request using "test.json" json file and "/symbols" url and check "404" code

  Scenario: 06. Check that DELETE request is unable on page /quotes
    Then Send DELETE request using "test.json" json file and "/quotes" url and check "404" code

  Scenario: 07. Check that DELETE request is unable on page /symbols
    Then Send DELETE request using "test.json" json file and "/symbols" url and check "404" code
