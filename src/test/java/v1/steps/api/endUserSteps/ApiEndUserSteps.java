package v1.steps.api.endUserSteps;


import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import v1.v1_api.HttpRequests;
import v1.v1_api.RestAssuredTests;
import java.io.IOException;
import java.net.URISyntaxException;


public class ApiEndUserSteps
        extends ScenarioSteps {

    public HttpRequests httpRequests = new HttpRequests();
    public RestAssuredTests restAssuredTests = new RestAssuredTests();


    @Step
    public void sendPOSTRequestUsingJsonFileAndUrlAndCheckCode(String fileName, String url, String code) throws URISyntaxException, IOException {
        restAssuredTests.sendPOSTRequestUsingJsonFileAndUrlAndCheckCode(fileName, url, code);
    }

    @Step
    public void sendGETRequestWithUrlAndCheckCode(String url, String code) {
        restAssuredTests.sendGETRequestWithUrlAndCheckCode(url, code);
    }

    @Step
    public void sendDELETERequestUsingJsonFileAndUrlAndCheckCode(String fileName, String url, String code) throws URISyntaxException, IOException {
        restAssuredTests.sendDELETERequestUsingJsonFileAndUrlAndCheckCode(fileName, url, code);
    }

}