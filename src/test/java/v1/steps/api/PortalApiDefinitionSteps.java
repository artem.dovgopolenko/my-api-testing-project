package v1.steps.api;


import cucumber.api.java.en.*;
import net.thucydides.core.annotations.Steps;
import v1.steps.api.endUserSteps.ApiEndUserSteps;

public class PortalApiDefinitionSteps {

    @Steps(shared = true)
    ApiEndUserSteps user;


    @Then("^Send GET request with \"([^\"]*)\" url and check \"([^\"]*)\" code$")
    public void sendGETRequestWithUrlAndCheckCode(String url, String code) throws Throwable {
        user.sendGETRequestWithUrlAndCheckCode(url, code);
    }


    @Then("^Send POST request using \"([^\"]*)\" json file and \"([^\"]*)\" url and check \"([^\"]*)\" code$")
    public void sendPOSTRequestUsingJsonFileAndUrlAndCheckCode(String fileName, String url, String code) throws Throwable {
        user.sendPOSTRequestUsingJsonFileAndUrlAndCheckCode(fileName, url, code);
    }

    @Then("^Send DELETE request using \"([^\"]*)\" json file and \"([^\"]*)\" url and check \"([^\"]*)\" code$")
    public void sendDELETERequestUsingJsonFileAndUrlAndCheckCode(String fileName, String url, String code) throws Throwable {
        user.sendDELETERequestUsingJsonFileAndUrlAndCheckCode(fileName, url, code);
    }



}
