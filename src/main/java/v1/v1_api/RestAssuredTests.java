package v1.v1_api;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class RestAssuredTests extends RestAssuredMethods {
    private Map<Integer, String> quoteRefList = new ConcurrentHashMap<>();



    private MessageStorageService service;

    {
        try {
            service = new MessageStorageService("src/test/resources/files/metrics");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendPOSTRequestUsingJsonFileAndUrlAndCheckCode
            (String fileName,
             String url,
             String statusCode) throws URISyntaxException {
        RestAssured.registerParser("text/plain", Parser.JSON);
        Response res = sendPostUsingDefaultURL(url, fileName);
        res.then().statusCode(Integer.parseInt(statusCode));
//        res.then().body("name", Matchers.not(Matchers.empty()));
//        res.then().body("job", Matchers.not(Matchers.empty()));

    }

    public void sendDELETERequestUsingJsonFileAndUrlAndCheckCode
            (String fileName,
             String url,
             String statusCode) throws URISyntaxException {
        RestAssured.registerParser("text/plain", Parser.JSON);
        Response res = sendDeleteUsingDefaultURL(url, fileName);
        res.then().statusCode(Integer.parseInt(statusCode));
    }

    public void sendGETRequestWithUrlAndCheckCode(String url, String statusCode) {
        Response res = sendGET(url);
        res.then().statusCode(Integer.parseInt(statusCode));
    }

    public void postSequentialRequestsForUsersWithURLAndFILEPATHAndJSONFILEGetQuoteReferenceFromResponse(int numOfUsers, String url, String filePath, String fileName) throws URISyntaxException, IOException {
        for (int i = 0; i < numOfUsers; i++) {
            postRequestWithURLAndFilePathAndJSONFILEAndGetQuoteReferenceFromResponse(i, url, filePath, fileName);
        }
    }

    public Response postRequestWithURLAndFilePathAndJSONFILEAndGetQuoteReferenceFromResponse(int numOfRequests, String url, String filePath, String fileName) throws URISyntaxException {
        Response res = sendPostUsingDefaultURLFilePathAndFileName(url, filePath, fileName);
        JsonPath jsonPathEvaluator = res.jsonPath();
        String quoteRef = jsonPathEvaluator.get("quoteReference");
        System.out.println("API - quoteReference " + (numOfRequests + 1) + " : " + quoteRef);
        quoteRefList.put((numOfRequests + 1), quoteRef);
        return res;
    }

}


