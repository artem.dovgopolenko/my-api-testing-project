package v1.v1_api;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HttpRequests {



    public static String readFile(String filename) {
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public void sendPost() throws Exception {

        String url = "";
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

        URL resource = this.getClass().getResource("/files/test.json");
        String filePath = new File(resource.toURI()).getAbsolutePath();
        String postData = readFile(filePath);

        urlParameters.add(new BasicNameValuePair("data", postData));
        StringEntity se = new StringEntity(postData);
//    post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Username", "test.user@gmail.com");
        post.setHeader("Key", "11223344555");

        post.setEntity(se);

        HttpResponse response = httpClient.execute(post);
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + post.getEntity());
        int responseCode = response.getStatusLine().getStatusCode();
        System.out.println("Response Code : " + responseCode);
        if (responseCode == 200) {
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());
        }
    }


}

