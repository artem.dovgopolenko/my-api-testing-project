package v1.v1_api;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public final class MessageStorageService {

    public MessageStorageService(String messageStorageRoot) throws Exception {
        this.messageStorageRoot = messageStorageRoot;
        init();
    }

    private String messageStorageRoot;
    

    private File resolveFileToStore(String path) {
        File file = new File(path);

        int i = 0;
        while (file.exists()) {
            StringBuilder pathBuilder = new StringBuilder(path);
            String version = "(" + ++i + ")";
            if (path.contains(".")) {
                int offset = path.lastIndexOf(".");
                pathBuilder.insert(offset, version);
            } else {
                pathBuilder.append(version);
            }
            file = new File(pathBuilder.toString());
        }
        return file;
    }


    public void init() throws Exception {
        File file = new File("");
        messageStorageRoot = file.getAbsolutePath() + "/" + messageStorageRoot;
        messageStorageRoot = validateFolder(messageStorageRoot);
    }

    private String validateFolder(String path) throws IOException {
        File file = new File(path);
        if (!file.exists()) {
            System.out.println("Folder " + path + " doesn't exist, creating...");
            boolean result = file.mkdirs();
            if (!result) {
                throw new IOException("Cant create folder " + path);
            }
            System.out.println("Folder " + path + " created successfully");
        }
        return file.getAbsolutePath();
    }
}