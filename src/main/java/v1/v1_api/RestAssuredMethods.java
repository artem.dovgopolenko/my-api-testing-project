package v1.v1_api;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONException;
import v1.utils.PropertyManager;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.net.URISyntaxException;
import java.net.URL;


import static io.restassured.RestAssured.given;

public class RestAssuredMethods {

    private MessageStorageService service;

    {
        try {
            service = new MessageStorageService("src/test/resources/files/metrics");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String apiURL = PropertyManager.getInstance().getUrl();
    private static String apiURL2 = "";
    private static String userName = "test.user@uberbilling.com";
    private static String xGatewayKey = "11223344555";


    public static String readFile(String filename) {
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String readFileForPOSTRequest(String name) throws URISyntaxException {
        URL resource = this.getClass().getResource("/files/api/" + name);
        String filePath = new File(resource.toURI()).getAbsolutePath();
        return readFile(filePath);
    }

    public String readFileByFolderAndName(String folder, String name) throws URISyntaxException {
        URL resource = this.getClass().getResource(folder + name);
        String filePath = new File(resource.toURI()).getAbsolutePath();
        return readFile(filePath);
    }

    public Response sendGET(String requestURL) throws JSONException {
        RestAssured.baseURI = apiURL + requestURL;
        System.out.println("\nSending 'GET' request to URL : " + RestAssured.baseURI);
        Response res = given()
                .header("X-Consumer-Username", userName)
                .header("X-Gateway-Key", xGatewayKey)
                .contentType(ContentType.JSON)
                .when()
                .get(RestAssured.baseURI);
        System.out.println("Response Code : " + res.statusCode());
        System.out.println("Response Body : " + res.getBody().asString());
        return res;
    }
    public Response sendPostUsingDefaultURL(String requestURL, String fileName) throws JSONException, URISyntaxException {
        String postData = readFileForPOSTRequest(fileName);
        RestAssured.baseURI = apiURL + requestURL;
        System.out.println("\nSending 'POST' request to URL : " + RestAssured.baseURI);
        Response res = given()
//                .header("X-Consumer-Username", userName)
//                .header("X-Gateway-Key", xGatewayKey)
                .contentType(ContentType.JSON).
                        body(postData).
                        when().
                        post("");
        System.out.println("Response Code : " + res.statusCode());
        System.out.println("Headers: " + res.getHeaders());
        System.out.println("Response Body : " + res.getBody().asString());
        return res;
    }



    public Response sendPostUsingDefaultURLFilePathAndFileName(String requestURL, String filePath, String fileName) throws JSONException, URISyntaxException {
        String postData = readFileByFolderAndName(filePath, fileName);
//        RestAssured.baseURI = apiURL + requestURL;
        System.out.println("\nSending 'POST' request to URL : " + apiURL + requestURL);
        Response res = given()
                .header("X-Consumer-Username", userName)
                .header("X-Gateway-Key", xGatewayKey)
                .contentType(ContentType.JSON).
                        body(postData).
                        when().
                        post(apiURL + requestURL);
        System.out.println("Response Code : " + res.statusCode());
        System.out.println("Response Body : " + res.getBody().asString());
        return res;
    }

    public Response sendDeleteUsingDefaultURL(String requestURL, String fileName) throws JSONException, URISyntaxException {
        String postData = readFileForPOSTRequest(fileName);
        RestAssured.baseURI = apiURL + requestURL;
        System.out.println("\nSending 'Delete' request to URL : " + RestAssured.baseURI);
        Response res = given()
//                .header("X-Consumer-Username", userName)
//                .header("X-Gateway-Key", xGatewayKey)
                .contentType(ContentType.JSON).
                        body(postData).
                        when().
                        delete("");
        System.out.println("Response Code : " + res.statusCode());
        System.out.println("Headers: " + res.getHeaders());
        System.out.println("Response Body : " + res.getBody().asString());
        return res;

}
}

