package v1.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

    public static String getPropertyFilePath() {
        String propertyFilePath = null;
        switch (System.getProperty("os.name")) {
            case "Linux":
                propertyFilePath = "/src/test/resources/properties/";
                break;
            case "Windows 10":
                propertyFilePath = "\\src\\test\\resources\\properties\\";
                break;
        }
        return propertyFilePath;
    }

    private static PropertyManager instance;
    private static final Object lock = new Object();
    private static final String propertyFilePath = System.getProperty("user.dir") + getPropertyFilePath() + "env.properties";
    private static String url;

    public static PropertyManager getInstance() {
        if (instance == null) {
            synchronized (lock) {
                instance = new PropertyManager();
                instance.loadData();
            }
        }
        return instance;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        PropertyManager.url = url;
    }

    private void loadData() {
        Properties prop = new Properties();

        try {
            prop.load(new FileInputStream(propertyFilePath));

        } catch (IOException e) {
            System.out.println("Configuration properties file cannot be found");
        }

        url = prop.getProperty("url");

    }

}
