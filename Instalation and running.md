Tests Report you can find by link
https://artem.dovgopolenko.gitlab.io/my-api-testing-project/master/ 
Report will be available in several minutes after finishing job



Requirements for launch

Java 8
Maven 3.6.0
Google Chrome v88.0.4324.182
ChromeDriver v88.0.4324.96


Installation

Note:
The installation instructions are described for Ubuntu and partially for Mac OS.
Installation of required software can be different for other operation systems.



Note:
if you don't have ownership on /opt directory, set it with the following command.

$ sudo chown username.usergroup -R /opt

Java 8 (JDK)


Download JDK
1.1 Open link and download jdk-8u152-linux-x64.tar.gz
1.2 Unzip jdk-8u152-linux-x64.tar.gz  to /opt directory


Create symlinks
 $ sudo ln -fs /opt/jdk1.8.0_151 /opt/jdk1.8
 $ sudo update-alternatives --install /usr/bin/java java /opt/jdk1.8/bin/java 100500


Add JAVA_HOME environment variable to system:
3.1 Open /etc/environment 
 $ sudo nano /etc/environment
3.2 Append line JAVA_HOME=/opt/jdk1.8
3.3 Load the variable
 $ source /etc/environment



Maven 3.6.0


Download Maven
 $ wget http://www-eu.apache.org/dist/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz -O /opt/apache-maven-3.6.0-bin.tar.gz       


Extract the downloaded archive
 $ tar -xvzf /opt/apache-maven-3.6.0-bin.tar.gz -C /opt/


Create symlinks
 $ sudo ln -fs /opt/apache-maven-3.6.0 /opt/maven
 $ sudo ln -fs /opt/maven/bin/mvn /usr/bin/mvn



Google Chrome


Add key
 $ wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -


Set repository
 $ echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list


Install package
 $ sudo apt-get update
 $ sudo apt-get install google-chrome-stable



ChromeDriver


Ubuntu


Download ChromeDriver
 $ mkdir -p /opt/selenium
 $ wget --no-verbose -O /opt/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/2.33/chromedriver_linux64.zip


Unzip
 $ unzip /opt/chromedriver_linux64.zip -d /opt/selenium
 $ rm /opt/chromedriver_linux64.zip


Make ChromeDriver executable
 $ chmod 755 /opt/selenium/chromedriver


Create symlinks
 $ sudo ln -fs /opt/selenium/chromedriver /usr/bin/chromedriver


Test execution


Download project
 $ git clone https://gitlab.com/artem.dovgopolenko/my-api-testing-project.git


Open project folder
 $ cd my-api-testing-project


Run maven
 $ mvn clean verify

When the tests are finished, open report
 $ google-chrome /{project_dir}/target/site/serenity/index.html

How to run tests on GitLab Ci
 Go to https://gitlab.com/artem.dovgopolenko/my-api-testing-project
 Go to CI/CD --> Pipelines
 Click on "Run pipeline" button, then on opened page click "Run pipeline" button
 Test report available on https://artem.dovgopolenko.gitlab.io/my-api-testing-project/master/   
 Report will be available in several minutes after finishing job